package adisho.server;

import java.util.List;
import java.util.Collection;
import java.util.LinkedList;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class RgServerResponse {

	@JsonProperty("traceId")
	protected String traceId;

	@JsonProperty("response")
	protected Object response;

	@JsonProperty("warnings")
	protected List<Object> warnings;

	@JsonProperty("errors")
	protected List<Object> errors;
	
	public RgServerResponse() {
		// ==rg== generate random traceId
		this.traceId = "Random trace id";
	}
	
	public RgServerResponse(String traceId) {
		this.traceId = traceId;
	}
	
	public RgServerResponse withReponse(Object response) {
		this.response = response;
		return this;
	}
	
	public RgServerResponse withWarning(Object warning) {
		if (this.warnings == null) {
			this.warnings = new LinkedList<>();
		}
		this.warnings.add(warning);
		return this;
	}
	
	public RgServerResponse withErrors(Collection<Object> errors) {
		if (this.errors == null) {
			this.errors = new LinkedList<>();
		}
		this.errors.addAll(errors);
		return this;
	}
	
	public RgServerResponse withError(Object error) {
		if (this.errors == null) {
			this.errors = new LinkedList<>();
		}
		this.errors.add(error);
		return this;
	}
}
