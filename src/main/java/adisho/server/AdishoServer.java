package adisho.server;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.servlet.ServletContainer;

public class AdishoServer {

	private static Logger logger = LogManager.getLogger();	
	
	private Server server = null;
	private List<Handler> handlers = new LinkedList<>();
	private Map<String, ServletContextHandler> endpointContexts = new HashMap<>();
	
	private int port = 80;
	
	private StringBuilder initMessage = new StringBuilder("Server initialize log:\n");
	
	public AdishoServer() {
        setShutdownHook();
	}

	public AdishoServer withPort(int port) {
		this.port = port;
		return this;
	}
	
	public AdishoServer withResourceHandler(String basePath, String resoucesDirectory) {
        ResourceHandler resourceHandler = new ResourceHandler();
        resourceHandler.setResourceBase(resoucesDirectory);
        resourceHandler.setDirectoriesListed(false);
        ContextHandler resourceContext = new ContextHandler(basePath);
        resourceContext.setHandler(resourceHandler);
        this.handlers.add(resourceContext);

		initMessage.append("Resource: `" + basePath + "` --> `" + resoucesDirectory + "`\n");

		return this;
	}
	
	public AdishoServer withEndpointHandler(Class<?> clazz, String context, String path) {
		String className = clazz.getCanonicalName();
		ServletContextHandler endpointContext = getEndpointContext(context);
		ServletHolder servletHolder = endpointContext.addServlet(ServletContainer.class, path);
		servletHolder.setInitOrder(1);
		servletHolder.setInitParameter("jersey.config.server.provider.classnames", className);

		initMessage.append("route: `" + path + "` --> `" + className + "`\n");
		
		return this;
	}
	
	public AdishoServer start() throws Exception {
		this.server = new Server(port);
		
        HandlerCollection handlerCollection = new HandlerCollection();
        handlerCollection.setHandlers(this.handlers.toArray(new Handler[this.handlers.size()]));
        this.server.setHandler(handlerCollection);

		this.server.start();
		initMessage.append("port: " + port + "\n");
		initMessage.append("Server started");
		logger.info(initMessage.toString());
		initMessage = null;
		
		return this;
	}
	
	public void stop() {
		if (server != null) {
			logger.info("Going down");
			try {
				server.stop();
				server.join();
				server.destroy();
				server = null;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	private ServletContextHandler getEndpointContext(String context) {
		ServletContextHandler contextHandler = this.endpointContexts.get(context);
		if (contextHandler == null) {
			contextHandler = new ServletContextHandler(ServletContextHandler.SESSIONS);
			contextHandler.setContextPath(context);
	        this.handlers.add(contextHandler);
	        this.endpointContexts.put(context, contextHandler);
		}
		return contextHandler;
	}
	
	private void setShutdownHook() {
		AdishoServer server = this;
		Runtime.getRuntime().addShutdownHook(new Thread()
        {
            @Override
            public void run()
            {
            	server.stop();
            }
        });
	}
	
}
