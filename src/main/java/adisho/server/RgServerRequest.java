package adisho.server;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown=true)
public class RgServerRequest {

	@JsonProperty("traceId")
	private String traceId;

	public synchronized String getTraceId() {
		if (this.traceId == null) {
			this.traceId = UUID.randomUUID().toString();
		}
		return this.traceId;
	}
}
