package adisho.server;

import java.util.LinkedList;
import java.util.Queue;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/")
public class TestEndPoint {

	static public Queue<TestEntity> entities = new LinkedList<>();
	
	static public void addEntity(TestEntity entitiy) {
		entities.add(entitiy);
	}

	static public void clearEntities() {
		entities.clear();
	}

    @GET
    @Path("test")
    @Produces(MediaType.APPLICATION_JSON)
    public TestEntity test() {
		return entities.poll();
    }
}
