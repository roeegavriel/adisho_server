package adisho.server;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

public class TestEntity {
	
	static final ObjectMapper mapper = new ObjectMapper();

	@JsonProperty("key")
	private String key;
	
	@JsonProperty("value")
	private int value;

	TestEntity() {
	}
	
	public TestEntity(String key, int value) {
		this.key = key;
		this.value = value;
	}
	
	public static TestEntity fromJson(String json) throws Exception {
		return mapper.readValue(json, TestEntity.class);
	}

	public String getKey() {
		return key;
	}

	public int getValue() {
		return value;
	}
		
}
